#! /usr/bin/env python2
# please python interpreter, be kind and use this encoding: utf-8 otherwise it will be total garbage :)

# system imports
import sys
import os
# Qt
from PyQt5 import QtCore, QtWidgets
from mainwindow import UiMainWindow
# modules
from updateStatus import UpdateStatus
from settings import SettingsWindow
from createProject import NewProject
from project import project

base_path = "./"


class Main(UiMainWindow):
    """
    Main window
    """
    yaml_path = None
    config_path = 'eval3k.cfg'

    def __init__(self, dialog):
        """
        Initialize the main window.
        We set the kbd shortcuts, and also the differents slgnals/slots necessary for this window.
        """
        UiMainWindow.__init__(self)
        self.setupUi(dialog)
        self.clearDisp()
        self.loadConfig()

        # Connect "add" button with a custom function (addInputTextToListbox)
        # CONNECT BUTTONS
        self.load_btn.clicked.connect(self.startSearchBtn)
        self.new_btn.clicked.connect(self.createProjectBtn)
        self.update_logistics_btn.clicked.connect(self.updateLogisticsInfoBtn)
        self.update_status_btn.clicked.connect(self.updateStatusBtn)

        # CONNECT MENUS
        # Menu> Settings
        self.actionSettings.setShortcut('Ctrl+P')
        self.actionSettings.triggered.connect(self.prefs)

        # Menu> Quit
        # self.actionQuit.setShortcut('Ctrl+Q')
        self.actionQuit.triggered.connect(self.quit)

        # Menu> Refresh
        self.actionRefresh_listing.setShortcut('Ctrl+R')
        self.actionRefresh_listing.triggered.connect(self.forceRefresh)

        # Menu> New
        self.actionNew_project.setShortcut('Ctrl+N')
        self.actionNew_project.triggered.connect(self.createProjectBtn)

        self.project_list = {}
        self.findProjects()

    def loadConfig(self):
        """
        load the config from the config file.
        In our case, it loads the yaml path.
        """
        import ConfigParser
        conf = ConfigParser.RawConfigParser()
        try:
            conf.read(self.config_path)
            self.yaml_path = conf.get('Configuration', 'yamlpath')
            self.return_val_label.setText("Config loaded!")
        except:
            print "[warn] No config found! Using current dir as default yaml place."
            conf.add_section('Configuration')
            self.yaml_path = os.getcwd()
            conf.set('Configuration', 'yamlpath', self.yaml_path)
            with open(self.config_path, 'wb') as configfile:
                conf.write(configfile)
            print "file was created"
            self.return_val_label.setText("Config written in " + str(self.config_path))
        global base_path
        base_path = self.yaml_path

    def findProjects(self):
        """
        load in memory the projects list based on the YAML files matching the pattern <login_p>_<YYYYMMDD>.yaml
        It avoids slowing down everytime a new file is created if there's a lot of them.
        FIXME: correct the regex to match only the wanted pattern.
               '^([a-z][a-z0-9-_]+)_(\d{8})'
        """
        self.project_list = {}

        import re
        regex = re.compile('^(.+)_(\d{8})')  # we look for <login_n>_<YYYYMMDD>
        global base_path
        print base_path
        for filePath in os.listdir(base_path):
            if filePath.endswith('.yaml'):
                matches = regex.match(filePath)
                if matches:
                    username, upath = matches.groups()
                    self.project_list[username] = upath
        print "[info] " + str(len(self.project_list)) + " projects loaded!"

    def forceRefresh(self):
        """
        Force refresh the YAML files in cache
        Warning: may slow down the app.
        """
        print "[menu] force file list refresh"
        self.findProjects()

    @staticmethod
    def prefs():
        """
        spawn the preference/settings window
        """
        updateSettingsWindow = QtWidgets.QDialog()
        updateSettingsWindow.ui = SettingsWindow(updateSettingsWindow, self.config_path, self.yaml_path)
        updateSettingsWindow.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        updateSettingsWindow.exec_()

    def startSearchBtn(self):
        """
        Search a file matching the login specified in input.
        Should find something like <input>_<timestamp>.yaml
        :return:
        """
        def tab2list(tab):
            str = ''
            try:
                for item in tab:
                    str += item + '\n'
            except TypeError:
                str = 'None.'
            return str

        print "[actn] search project"
        self.clearDisp()
        if self.search_login_lineedit.text():
            try:
                # TODO put here real YAML load
                global base_path
                # FIXME BUG Windows probable: verifier le path en utilisant les abstractions !

                if str(self.search_login_lineedit.text()) in self.project_list:
                    filename = str(base_path) + str(self.search_login_lineedit.text()) + "_" + str(self.project_list[self.search_login_lineedit.text()]) + ".yaml"
                    print "Path is " + filename
                    thisProj = project(filename)
                    self.thisProject = thisProj
                    self.name_label.setText(thisProj.name)
                    self.date_label.setText(thisProj.updatedate)
                    self.start_date_label.setText(thisProj.startdate)
                    self.chief_label.setText(thisProj.lead)
                    self.members_text.setPlainText(tab2list(thisProj.members))
                    self.isstartup_checkbox.setChecked(thisProj.isstartup)
                    self.follower_lineedit.setText(thisProj.followedby)
                    self.partners_text.setPlainText(tab2list(thisProj.partners))
                    self.town_following_label.setText(thisProj.townfollowing)
                    self.return_val_label.setText(filename + " loaded!")
                    self.status_progressbar.setValue(thisProj.getProgression())

                    print "=====> Updating table"
                    self.last_status_label.setText(thisProj.statuses[0][1])

                    for row in thisProj.statuses:
                        idx = thisProj.statuses.index(row)
                        self.tableWidget.insertRow(idx)
                        self.tableWidget.setItem(idx, 0, QtWidgets.QTableWidgetItem(str(row[0])))
                        self.tableWidget.setItem(idx, 1, QtWidgets.QTableWidgetItem(str(row[1])))
                        self.tableWidget.setItem(idx, 2, QtWidgets.QTableWidgetItem(str(row[2])))
                    print "End update !"

                    print thisProj.statuses
                else:
                    self.return_val_label.setText("invalid login!")
            except KeyError as e:
                import os.path
                if os.path.isfile(filename):
                    self.return_val_label.setText("Malformated YAML!")
                    print "[err] malformated YAML (" + str(filename) + "), no match for mandatory key " + str(e)
                else:
                    self.return_val_label.setText("No match!")
                    print "No match for " + str(e)
            except AttributeError as e:
                self.return_val_label.setText(str(filename) + " not found!")
                print "[err] unable to open file, is path correct? " + str(e)
        else:
            self.return_val_label.setText("Empty search!")

    @staticmethod
    def createProjectBtn():
        """
        spawn the new project window
        """
        print "[actn|menu] spawn window to create project"
        createProjectWindow = QtWidgets.QDialog()
        createProjectWindow.ui = NewProject(createProjectWindow)
        createProjectWindow.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        createProjectWindow.exec_()

    def updateLogisticsInfoBtn(self):
        """
        update the loaded YAML file with logistics info
        TODO: NOT IMPLEMENTED!
        """
        print "[actn] update logistics info"

        def str2tab(str):
            # fonction a tester
            tab = str.split(',').rstrip()
            return tab
        
        def tab2list(tab):
            str = ''
            try:
                for item in tab:
                    str += item + '\n'
            except TypeError:
                str = 'None.'
            return str

        members = set(self.members_text.toPlainText().split('\n'))
        try:
            members.remove(self.chief_label.text())
        except:
            pass  # no duplicate, everything ok
        print members

        # print ','.join(self.members_text.toPlainText().split('\n')).rstrip(',')

    def updateStatusBtn(self):
        """
        create the new status for project window
        """
        print "[actn] spawn window to update status"
        updateStatusWindow = QtWidgets.QDialog()
        updateStatusWindow.ui = UpdateStatus(updateStatusWindow, self.thisProject)
        updateStatusWindow.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        updateStatusWindow.exec_()
        self.startSearchBtn()

    @staticmethod
    def quit():
        """
        Close app.
        Also should warn if modifications were done to opened YAML file and ask for saving them.
        """
        print "[actn] quit"
        sys.exit()

    def clearDisp(self):
        """
        Function to call at app start and after search before result display
        """
        print "[dbg] CLEAR"
        self.return_val_label.setText("")
        self.last_status_label.setText("")
        self.status_progressbar.setValue(0)
        self.status_progressbar.setDisabled(True)
        self.name_label.setText("")
        self.date_label.setText("")
        self.start_date_label.setText("")
        self.chief_label.setText("")
        self.members_text.setPlainText("")
        self.town_following_label.setText("")
        self.isstartup_checkbox.setChecked(False)
        self.follower_lineedit.setText("")
        self.partners_text.setPlainText("")
        for item in range(0, int(self.tableWidget.rowCount())):
            self.tableWidget.removeRow(0)

        self.update_logistics_btn.setDisabled(True)
        # self.update_status_btn.setDisabled(True)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    dialog = QtWidgets.QDialog()

    prog = Main(dialog)

    dialog.show()
    sys.exit(app.exec_())
