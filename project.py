#! /usr/bin/env python2
# please python interpreter, be kind and use this encoding: utf-8 otherwise it will be total garbage :)


class project():
    """
    Project's object
    """
    def __init__(self, filename):
        """
        Init the project object.
        Set a few vars from loaded file <filename>
        """
        import yaml
        self.yamlFile = filename
        try:
            with open(self.yamlFile) as fd:
                projectData = yaml.safe_load(fd)
        except IOError as e:
            print "[err] problem opening " + str(e)
            return
        # try:
        self.path = filename
        self.name = projectData['project']['proj_name']
        self.lead = projectData['project']['lead']
        self.startdate = projectData['logistics']['start_date']
        self.updatedate = projectData['logistics']['update_date']
        self.members = projectData['logistics']['members']
        self.isstartup = projectData['logistics']['startup']
        self.partners = projectData['logistics']['partnership']
        self.townfollowing = projectData['logistics']['town']
        self.followedby = projectData['logistics']['follower']
        self.statuses = list()

        for etat in projectData['statuses']:
            try:
                for entry in projectData['statuses'][etat]:
                    self.statuses.append([str(entry), str(etat), str(projectData['statuses'][etat][entry])])
            except TypeError:
                pass
        self.statuses.sort(key=lambda x: x[0], reverse=True)  # tri par date decroissante (plus recent en premier)

        print '[dbg] YAML loaded.'

        # except:
        #     print '[warn] malformated YAML file : '

        # print '[dbg] loaded from YAML: '
        # print 'name= ' + self.name
        # print 'lead= ' + self.lead
        # print 'start= '+self.startdate
        # print 'update='+self.updatedate
        # print 'members'
        # print self.members
        # print 'startup' + str(self.isstartup)
        # print 'partners'
        # print self.partners
        # print 'town= '+self.townfollowing
        # print 'follower=' + self.followedby
        self.getProgression()

    def getProgression(self):
        """
        get the project's progression
        """
        statuses = ['Pitch', 'CDC', 'Suivi', 'Prototype', 'Communication', 'Defense', 'Intranet']
        current_status = str(self.statuses[0][1])
        for status in enumerate(statuses):
            if str(status[1]) == str(current_status):
                return int(status[0])
        return 0
