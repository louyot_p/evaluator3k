#! /usr/bin/env python2
# please python interpreter, be kind and use this encoding: utf-8 otherwise it will be total garbage :)
from PyQt5.QtWidgets import QMessageBox
import datetime
from newProject import UiNewProject
global base_path
base_path = './'


class NewProject(UiNewProject):
    """
    New project Window.
    """
    def __init__(self, dialog):
        """
        Init the new project window and prepare a skeleton for the new YAML to be commited.
        """
        UiNewProject.__init__(self)
        self.setupUi(dialog)
        self.dialog = dialog

        self.skeldata = {'logistics': {'follower': str(),
                                       'members': None,
                                       'partnership': None,
                                       'start_date': str(datetime.date.today().strftime("%Y%m%d")),
                                       'startup': False,
                                       'town': str(),
                                       'update_date': str(datetime.date.today().strftime("%Y%m%d"))
                                       },
                         'project': {'lead': str(),
                                     'proj_name': str()
                                     },
                         'statuses': {'CDC': None,
                                      'Communication': None,
                                      'Defense': None,
                                      'Intranet': None,
                                      'Pitch': dict(),
                                      'Prototype': None,
                                      'Suivi': None}}

        # Signal/Slot
        self.create_btn.clicked.connect(self.createNewProject)

    def validateEntries(self):
        """
        Function to validate entries when creating a new project.
        Mandatory fields are : Project name, project leader, project town.
        TODO: check if file can be created (not already exist + leader not already leader of another project)
        """
        entries_valid = True
        if len(self.name_edit.text()) < 3:
            entries_valid = False
            self.label_3.setStyleSheet("QLabel { color : red; }")

        # FIXME: Faire une verification de login ([a-z0-9-]+_[a-z0-9]{1})
        if len(self.leader_edit.text()) == 0:
            entries_valid = False
            self.label.setStyleSheet("QLabel { color : red; }")

        if len(self.town_edit.text()) < 3:
            entries_valid = False
            self.label_4.setStyleSheet("QLabel { color : red; }")

        # FIXME: Verifier que le fichier a creer n'existe pas, sinon reporter nom du projet deja en cours + ERREUR
        import os
        self.filename = str(self.leader_edit.text()) + "_" + str(datetime.date.today().strftime("%Y%m%d")) + ".yaml"
        if os.path.exists(base_path + self.filename):
            entries_valid = False
            print "[err] File already exists !!"
        # FIXME: Verifier que le chef de proj n'a pas deja un projet en cours et le reporter

        return entries_valid

    def createNewProject(self):
        """
        Commit a new project to a YAML file.
        Every check must be done **BEFORE** this function as it **WRITES** the YAML file.
        """
        import yaml

        if self.validateEntries():
            print "VALID !"
            global base_path
            # FIXME utiliser l'abstraction de python pour les path
            # print self.filename
            self.skeldata['project']['lead'] = str(self.leader_edit.text())
            self.skeldata['project']['proj_name'] = str(self.name_edit.text())
            self.skeldata['logistics']['startup'] = bool(self.startup_chkbox.checkState())
            self.skeldata['logistics']['follower'] = str(self.follower_edit.text())
            self.skeldata['logistics']['town'] = str(self.town_edit.text())
            self.skeldata['statuses']['Pitch'][str(datetime.date.today().strftime("%Y%m%d"))] = str(self.comment_edit.text())
            self.skeldata['logistics']['members'] = []
            for mb in self.member_text.toPlainText().split('\n'):
                self.skeldata['logistics']['members'].append(str(mb))
            self.skeldata['logistics']['partnership'] = []
            for partn in self.partner_text.toPlainText().split('\n'):
                self.skeldata['logistics']['partnership'].append(str(partn))

            with open(base_path + self.filename, 'w') as fd:
                fd.write(yaml.dump(self.skeldata, default_flow_style=False, indent=4))
            # window is not needed anymore, so we close it.
            self.dialog.close()
        else:
            # Argument invalid or missing argument.
            print "Argument INVALID!"
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Some details are missing.")
            msg.setInformativeText("Mandatory fields are login, project name and town.")
            msg.setWindowTitle("Project creation impossible")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
