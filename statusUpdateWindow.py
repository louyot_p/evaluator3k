# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'statusUpdateWindow.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class UiStatusUpdate(object):
    def setupUi(self, status_update):
        status_update.setObjectName("status_update")
        status_update.resize(384, 323)
        self.groupBox = QtWidgets.QGroupBox(status_update)
        self.groupBox.setGeometry(QtCore.QRect(10, 90, 361, 181))
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.groupBox.setFont(font)
        self.groupBox.setObjectName("groupBox")
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setGeometry(QtCore.QRect(10, 80, 121, 31))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.comment_edit = QtWidgets.QLineEdit(self.groupBox)
        self.comment_edit.setGeometry(QtCore.QRect(10, 130, 351, 32))
        self.comment_edit.setMaxLength(140)
        self.comment_edit.setObjectName("comment_edit")
        self.prev_status_abel = QtWidgets.QLabel(self.groupBox)
        self.prev_status_abel.setGeometry(QtCore.QRect(140, 80, 171, 31))
        self.prev_status_abel.setObjectName("prev_status_abel")
        self.proto_btn = QtWidgets.QPushButton(self.groupBox)
        self.proto_btn.setGeometry(QtCore.QRect(160, 40, 51, 30))
        self.proto_btn.setObjectName("proto_btn")
        self.cdc_btn = QtWidgets.QPushButton(self.groupBox)
        self.cdc_btn.setGeometry(QtCore.QRect(60, 40, 51, 30))
        self.cdc_btn.setObjectName("cdc_btn")
        self.intra_btn = QtWidgets.QPushButton(self.groupBox)
        self.intra_btn.setGeometry(QtCore.QRect(310, 40, 51, 30))
        self.intra_btn.setObjectName("intra_btn")
        self.suivi_btn = QtWidgets.QPushButton(self.groupBox)
        self.suivi_btn.setGeometry(QtCore.QRect(110, 40, 51, 30))
        self.suivi_btn.setObjectName("suivi_btn")
        self.pitch_btn = QtWidgets.QPushButton(self.groupBox)
        self.pitch_btn.setGeometry(QtCore.QRect(10, 40, 51, 30))
        self.pitch_btn.setFlat(False)
        self.pitch_btn.setObjectName("pitch_btn")
        self.comm_btn = QtWidgets.QPushButton(self.groupBox)
        self.comm_btn.setGeometry(QtCore.QRect(210, 40, 51, 30))
        self.comm_btn.setObjectName("comm_btn")
        self.def_btn = QtWidgets.QPushButton(self.groupBox)
        self.def_btn.setGeometry(QtCore.QRect(260, 40, 51, 30))
        self.def_btn.setObjectName("def_btn")
        self.label = QtWidgets.QLabel(status_update)
        self.label.setGeometry(QtCore.QRect(10, 20, 111, 20))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.projec_name_lbl = QtWidgets.QLabel(status_update)
        self.projec_name_lbl.setGeometry(QtCore.QRect(150, 20, 191, 20))
        self.projec_name_lbl.setText("")
        self.projec_name_lbl.setObjectName("projec_name_lbl")
        self.leader_name_lbl = QtWidgets.QLabel(status_update)
        self.leader_name_lbl.setGeometry(QtCore.QRect(150, 50, 191, 20))
        self.leader_name_lbl.setText("")
        self.leader_name_lbl.setObjectName("leader_name_lbl")
        self.label_5 = QtWidgets.QLabel(status_update)
        self.label_5.setGeometry(QtCore.QRect(10, 50, 111, 20))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.save_btn = QtWidgets.QPushButton(status_update)
        self.save_btn.setGeometry(QtCore.QRect(264, 280, 111, 30))
        self.save_btn.setObjectName("save_btn")

        self.retranslateUi(status_update)
        QtCore.QMetaObject.connectSlotsByName(status_update)

    def retranslateUi(self, status_update):
        _translate = QtCore.QCoreApplication.translate
        status_update.setWindowTitle(_translate("status_update", "Update Status"))
        self.groupBox.setTitle(_translate("status_update", "Status"))
        self.label_2.setText(_translate("status_update", "Previous status"))
        self.comment_edit.setToolTip(_translate("status_update", "<html><head/><body><p>140 car. max</p></body></html>"))
        self.prev_status_abel.setText(_translate("status_update", "unknown"))
        self.proto_btn.setText(_translate("status_update", "Proto"))
        self.cdc_btn.setText(_translate("status_update", "CDC"))
        self.intra_btn.setText(_translate("status_update", "Intra"))
        self.suivi_btn.setText(_translate("status_update", "Suivi"))
        self.pitch_btn.setText(_translate("status_update", "Pitch"))
        self.comm_btn.setText(_translate("status_update", "Comm"))
        self.def_btn.setText(_translate("status_update", "Def"))
        self.label.setText(_translate("status_update", "Project\'s name"))
        self.label_5.setText(_translate("status_update", "Leader\'s login"))
        self.save_btn.setText(_translate("status_update", "Create entry"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    status_update = QtWidgets.QDialog()
    ui = UiStatusUpdate()
    ui.setupUi(status_update)
    status_update.show()
    sys.exit(app.exec_())

