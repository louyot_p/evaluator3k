# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'NewProject.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class UiNewProject(object):
    def setupUi(self, newProject):
        newProject.setObjectName("NewProject")
        newProject.resize(530, 449)
        self.groupBox = QtWidgets.QGroupBox(newProject)
        self.groupBox.setGeometry(QtCore.QRect(20, 130, 271, 181))
        self.groupBox.setObjectName("groupBox")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setGeometry(QtCore.QRect(20, 30, 67, 20))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setGeometry(QtCore.QRect(20, 60, 67, 20))
        self.label_2.setObjectName("label_2")
        self.leader_edit = QtWidgets.QLineEdit(self.groupBox)
        self.leader_edit.setGeometry(QtCore.QRect(150, 30, 101, 21))
        self.leader_edit.setObjectName("leader_edit")
        self.member_text = QtWidgets.QPlainTextEdit(self.groupBox)
        self.member_text.setGeometry(QtCore.QRect(150, 60, 101, 75))
        self.member_text.setPlainText("")
        self.member_text.setObjectName("member_text")
        self.startup_chkbox = QtWidgets.QCheckBox(self.groupBox)
        self.startup_chkbox.setGeometry(QtCore.QRect(20, 120, 95, 25))
        self.startup_chkbox.setObjectName("startup_chkbox")
        self.label_4 = QtWidgets.QLabel(self.groupBox)
        self.label_4.setGeometry(QtCore.QRect(20, 150, 131, 20))
        self.label_4.setObjectName("label_4")
        self.town_edit = QtWidgets.QLineEdit(self.groupBox)
        self.town_edit.setGeometry(QtCore.QRect(150, 150, 101, 21))
        self.town_edit.setObjectName("town_edit")
        self.label_3 = QtWidgets.QLabel(newProject)
        self.label_3.setGeometry(QtCore.QRect(20, 90, 101, 20))
        self.label_3.setObjectName("label_3")
        self.name_edit = QtWidgets.QLineEdit(newProject)
        self.name_edit.setGeometry(QtCore.QRect(130, 91, 381, 21))
        self.name_edit.setObjectName("name_edit")
        self.groupBox_2 = QtWidgets.QGroupBox(newProject)
        self.groupBox_2.setGeometry(QtCore.QRect(300, 130, 221, 181))
        self.groupBox_2.setObjectName("groupBox_2")
        self.label_5 = QtWidgets.QLabel(self.groupBox_2)
        self.label_5.setGeometry(QtCore.QRect(10, 30, 91, 20))
        self.label_5.setObjectName("label_5")
        self.follower_edit = QtWidgets.QLineEdit(self.groupBox_2)
        self.follower_edit.setGeometry(QtCore.QRect(100, 30, 101, 21))
        self.follower_edit.setObjectName("follower_edit")
        self.label_6 = QtWidgets.QLabel(self.groupBox_2)
        self.label_6.setGeometry(QtCore.QRect(10, 60, 67, 20))
        self.label_6.setObjectName("label_6")
        self.partner_text = QtWidgets.QPlainTextEdit(self.groupBox_2)
        self.partner_text.setGeometry(QtCore.QRect(100, 60, 101, 75))
        self.partner_text.setPlainText("")
        self.partner_text.setObjectName("partner_text")
        self.groupBox_3 = QtWidgets.QGroupBox(newProject)
        self.groupBox_3.setGeometry(QtCore.QRect(20, 310, 491, 91))
        self.groupBox_3.setObjectName("groupBox_3")
        self.label_7 = QtWidgets.QLabel(self.groupBox_3)
        self.label_7.setGeometry(QtCore.QRect(10, 40, 221, 20))
        self.label_7.setObjectName("label_7")
        self.comment_edit = QtWidgets.QLineEdit(self.groupBox_3)
        self.comment_edit.setGeometry(QtCore.QRect(10, 60, 481, 21))
        self.comment_edit.setObjectName("comment_edit")
        self.label_8 = QtWidgets.QLabel(newProject)
        self.label_8.setGeometry(QtCore.QRect(10, 10, 501, 61))
        font = QtGui.QFont()
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setWordWrap(True)
        self.label_8.setObjectName("label_8")
        self.create_btn = QtWidgets.QPushButton(newProject)
        self.create_btn.setGeometry(QtCore.QRect(420, 410, 95, 30))
        self.create_btn.setObjectName("create_btn")

        self.retranslateUi(newProject)
        QtCore.QMetaObject.connectSlotsByName(newProject)

    def retranslateUi(self, newProject):
        _translate = QtCore.QCoreApplication.translate
        newProject.setWindowTitle(_translate("NewProject", "New Project"))
        self.groupBox.setTitle(_translate("NewProject", "Logistics"))
        self.label.setText(_translate("NewProject", "Leader"))
        self.label_2.setText(_translate("NewProject", "Members"))
        self.startup_chkbox.setText(_translate("NewProject", "startup"))
        self.label_4.setText(_translate("NewProject", "Followed in (town)"))
        self.label_3.setText(_translate("NewProject", "Project\'s name"))
        self.groupBox_2.setTitle(_translate("NewProject", "Misc"))
        self.label_5.setText(_translate("NewProject", "Followed by"))
        self.label_6.setText(_translate("NewProject", "Partners"))
        self.groupBox_3.setTitle(_translate("NewProject", "Pitch"))
        self.label_7.setText(_translate("NewProject", "Comment (optionnal, 160char)"))
        self.label_8.setText(_translate("NewProject", "WARNING: before adding a new project, be sure to update the repository. Only one project in progress per person. Before adding a new project, the pitch must have happened."))
        self.create_btn.setText(_translate("NewProject", "Create"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    newProject = QtWidgets.QDialog()
    ui = UiNewProject()
    ui.setupUi(newProject)
    newProject.show()
    sys.exit(app.exec_())

