#! /usr/bin/env python2
# please python interpreter, be kind and use this encoding: utf-8 otherwise it will be total garbage :)
from statusUpdateWindow import UiStatusUpdate


class UpdateStatus(UiStatusUpdate):
    """
    Update Status Window.
    TODO: bind enter to the commit button.
    """
    def __init__(self, dialog, thisProject):
        """
        Init the status update window and sets a few vars for the magic.
        """
        UiStatusUpdate.__init__(self)
        self.thisProject = thisProject
        self.setupUi(dialog)
        self.dialog = dialog
        self.projStatut = ["Pitch", "CDC", "Suivi", "Prototype", "Communication", "Defense", "Intranet"]
        self.buttonstatus = -1
        self.prev_status_abel.setText(thisProject.statuses[0][1])
        # self.highlightStatus(self.thisProject.statuses[0][1])
        self.projec_name_lbl.setText(self.thisProject.name)
        self.leader_name_lbl.setText(self.thisProject.lead)

        # Signal/Slot
        self.pitch_btn.clicked.connect(lambda: self.buttonselected(0))
        self.cdc_btn.clicked.connect(lambda: self.buttonselected(1))
        self.suivi_btn.clicked.connect(lambda: self.buttonselected(2))
        self.proto_btn.clicked.connect(lambda: self.buttonselected(3))
        self.comm_btn.clicked.connect(lambda: self.buttonselected(4))
        self.def_btn.clicked.connect(lambda: self.buttonselected(5))
        self.intra_btn.clicked.connect(lambda: self.buttonselected(6))
        self.save_btn.clicked.connect(self.commitNewStatus)

    def highlightStatus(self, newStatus):
        """
        Highlighting status func.
        This function highlight the correct button (fletten it) in UI with an array of function pointers.
        :bug: FIXME the current architecture blocks the automated loading of current status at opening.
        """
        status = {"Pitch": self.pitch_btn,
                  "CDC": self.cdc_btn,
                  "Suivi": self.suivi_btn,
                  "Prototype": self.proto_btn,
                  "Communication": self.comm_btn,
                  "Defense": self.def_btn,
                  "Intranet": self.intra_btn}
        for it in status:
            status[it].setFlat(False)
        status[self.thisProject.statuses[0][1]].setFlat(True)

    def buttonselected(self, btn):
        """
        Toggle status on selected status button.
        TODO: cleanup! + prendre le status actuel en flat by default
        """
        print "btn status = " + str(self.buttonstatus)
        print "btn select = " + str(btn)

        if self.buttonstatus == 0:
            self.pitch_btn.setFlat(False)
        elif self.buttonstatus == 1:
            self.cdc_btn.setFlat(False)
        elif self.buttonstatus == 2:
            self.suivi_btn.setFlat(False)
        elif self.buttonstatus == 3:
            self.proto_btn.setFlat(False)
        elif self.buttonstatus == 4:
            self.comm_btn.setFlat(False)
        elif self.buttonstatus == 5:
            self.def_btn.setFlat(False)
        elif self.buttonstatus == 6:
            self.intra_btn.setFlat(False)

        if btn == 0:
            self.pitch_btn.setFlat(True)
        elif btn == 1:
            self.cdc_btn.setFlat(True)
        elif btn == 2:
            self.suivi_btn.setFlat(True)
        elif btn == 3:
            self.proto_btn.setFlat(True)
        elif btn == 4:
            self.comm_btn.setFlat(True)
        elif btn == 5:
            self.def_btn.setFlat(True)
        elif btn == 6:
            self.intra_btn.setFlat(True)
        self.buttonstatus = btn

    def commitNewStatus(self):
        """
        Append new status in opened YAML file.
        This function opens, load and rewrites the full YAML.
        """
        from datetime import date
        import yaml

        if self.buttonstatus == -1:
            print "[err] no status chosen!"
            return
        print "[actn] saving new status to YAML"
        insert_status = str(self.projStatut[self.buttonstatus])
        insert_date = str(date.today().strftime("%Y%m%d"))
        insert_comment = str(self.comment_edit.text())
        # Reload data from YAML
        with open(self.thisProject.path) as fd:
            datatab = yaml.safe_load(fd)
        # check whether there is already data for chosen status, otherwise specify to python it's a dict
        try:
            datatab['statuses'][insert_status][insert_date] = insert_comment
        except TypeError:
            datatab['statuses'][insert_status] = {}
            datatab['statuses'][insert_status][insert_date] = insert_comment
        # Write data to YAML
        with open(self.thisProject.path, 'w') as fd:
            fd.write(yaml.dump(datatab, default_flow_style=False, indent=4))
        print "[dbg] Data written!"
        self.dialog.close()
