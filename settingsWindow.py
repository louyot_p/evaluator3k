# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SettingsWindow.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class UiSettings(object):
    def setupUi(self, Settings):
        Settings.setObjectName("Settings")
        Settings.resize(400, 103)
        self.label = QtWidgets.QLabel(Settings)
        self.label.setGeometry(QtCore.QRect(20, 20, 111, 20))
        self.label.setObjectName("label")
        self.path_pick_result_input = QtWidgets.QLineEdit(Settings)
        self.path_pick_result_input.setGeometry(QtCore.QRect(160, 21, 151, 21))
        self.path_pick_result_input.setReadOnly(False)
        self.path_pick_result_input.setObjectName("path_pick_result_input")
        self.save_btn = QtWidgets.QPushButton(Settings)
        self.save_btn.setGeometry(QtCore.QRect(290, 60, 95, 30))
        self.save_btn.setObjectName("save_btn")
        self.toolButton = QtWidgets.QToolButton(Settings)
        self.toolButton.setGeometry(QtCore.QRect(320, 20, 26, 21))
        self.toolButton.setObjectName("toolButton")

        self.retranslateUi(Settings)
        QtCore.QMetaObject.connectSlotsByName(Settings)

    def retranslateUi(self, Settings):
        _translate = QtCore.QCoreApplication.translate
        Settings.setWindowTitle(_translate("Settings", "Settings"))
        self.label.setText(_translate("Settings", "YAML fies path"))
        self.save_btn.setText(_translate("Settings", "Save"))
        self.toolButton.setText(_translate("Settings", "..."))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Settings = QtWidgets.QDialog()
    ui = UiSettings()
    ui.setupUi(Settings)
    Settings.show()
    sys.exit(app.exec_())

