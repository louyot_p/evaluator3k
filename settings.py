#! /usr/bin/env python2
# please python interpreter, be kind and use this encoding: utf-8 otherwise it will be total garbage :)
from settingsWindow import UiSettings
from PyQt5 import QtCore, QtWidgets
import os


class SettingsWindow(UiSettings):
    """
    Update Settings Window.
    TODO: use the globvar base_path for yaml path or find a better way..
    """
    def __init__(self, dialog, configPath, yamlPath):
        """
        Init the settings window and set the signal/slots
        """
        UiSettings.__init__(self)
        self.config_path = configPath
        self.yaml_path = yamlPath

        self.setupUi(dialog)
        self.dialog = dialog

        self.path_pick_result_input.setText(self.yaml_path)
        # Signals/Slots
        self.save_btn.clicked.connect(self.saveSettings)
        self.toolButton.clicked.connect(self.selectDir)

    def saveSettings(self):
        """
        save settings to config file
        """
        print "[actn] should save settings"
        yamlPath = str(self.path_pick_result_input.text())
        print "path = " + yamlPath
        print "[dbg] config is " + self.config_path
        if yamlPath and os.path.isdir(yamlPath):
            # TODO update config (and load it somewhere)
            print "path is real"

            global base_path
            base_path = str(self.path_pick_result_input.text() + "/")
            import ConfigParser
            conf = ConfigParser.RawConfigParser()
            #try:
            conf.read(self.config_path)
            self.yaml_path = conf.get('Configuration', 'yamlpath')
            # FIXME BUG Probable windows, utiliser les abstractions de path !
            conf.set('Configuration', 'yamlpath', str(self.path_pick_result_input.text()))
            with open(self.config_path, 'w+') as configfile:
                conf.write(configfile)
            print "config updated"
            # except:
            #     print "[err] No config found! Shouldn't happen!!"
            #     sys.exit(-1)

            # with open(self.config_path, 'wb') as configfile:
            #     conf.write(configfile)
            # print "file was created"
            #    self.return_val_label.setText("Config written in " + str(self.config_path))
            self.dialog.close()
            print "should close"
        else:
            print "YAML dir doesn't exist"

    def selectDir(self):
        """
        spawn directory selection
        FIXME: some more bugs with on the fly update. dirPath is not propagated.
        """
        dirPath = QtWidgets.QFileDialog.getExistingDirectory(parent=self.dialog, caption="Select working dir")
        self.path_pick_result_input.setText(str(dirPath) + "/")
